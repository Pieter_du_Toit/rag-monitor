﻿using System;
using System.Data;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class Maintenance : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
            txtSecurity.Attributes["type"] = "password";
        }
    }

    protected void serverListTable_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        e.Cancel = true;
        serverListTable.EditIndex = -1;
        BindGrid();
    }

    protected void serverListTable_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int id = serverListTable.Rows[e.RowIndex].DataItemIndex;
        var ServerName = serverListTable.Rows[e.RowIndex].FindControl("txtServerName") as TextBox;
        var ServerIP = serverListTable.Rows[e.RowIndex].FindControl("txtServerIP") as TextBox;
        var TestIP = serverListTable.Rows[e.RowIndex].FindControl("txtTestIP") as TextBox;
        var TestDescription = serverListTable.Rows[e.RowIndex].FindControl("txtTestDescription") as TextBox;
        var TestInterval = serverListTable.Rows[e.RowIndex].FindControl("ddlTestInterval") as DropDownList;
        var TestType = serverListTable.Rows[e.RowIndex].FindControl("ddlTestType") as DropDownList;

        serverListTable.EditIndex = -1;

        BindGrid();

        var ds = serverListTable.DataSource as DataSet;

        if (ds != null)
        {
            if (ServerName != null) ds.Tables[0].Rows[id]["ServerName"] = ServerName.Text;
            if (ServerIP != null) ds.Tables[0].Rows[id]["ServerIP"] = ServerIP.Text;
            if (TestIP != null) ds.Tables[0].Rows[id]["TestIP"] = TestIP.Text;
            if (TestDescription != null) ds.Tables[0].Rows[id]["TestDescription"] = TestDescription.Text;
            if (TestInterval != null) ds.Tables[0].Rows[id]["TestInterval"] = TestInterval.Text;
            if (TestType != null) ds.Tables[0].Rows[id]["TestType"] = TestType.Text;
            if (TestInterval != null) ds.Tables[0].Rows[id]["TestInterval"] = TestInterval.Text;

            try
            {
                ds.WriteXml(Server.MapPath("/data/Servers.xml"));
            }
            catch
            {
                //Response.Redi
                Server.Transfer("/errorpages/AccessToServerConfigXML.aspx");
            }
        }

        BindGrid();
    }

    protected void serverListTable_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Edit":
                break;
            case "Cancel":
                break;
            case "Update":
                break;
        }
    }

    protected void serverListTable_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        serverListTable.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    protected void serverListTable_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Enabled = SecurityPassed();
        }
    }

    protected void btnActivate_Click(object sender, EventArgs e)
    {
        if (AllowAccess())
        {
            var btn = (Button)sender;

            var doc = new XmlDocument();
            doc.Load(Server.MapPath("/data/Servers.xml"));
            var ActiveStatus =
                doc.SelectSingleNode("Servers//Server[TestID='" + Convert.ToInt32(btn.CommandArgument) + "']/Active");
            ActiveStatus.InnerText = btn.CommandName == "1" ? "0" : "1";
            try
            {
                doc.Save(Server.MapPath("/data/Servers.xml"));
            }
            catch
            {
                Server.Transfer("/errorpages/AccessToServerConfigXML.aspx");
            }
            BindGrid();
        }
    }

    private bool AllowAccess()
    {
        return txtSecurity.Text == WebConfigurationManager.AppSettings["security"];
    }

    private bool SecurityPassed()
    {
        if (lblElevatedPermissions.Text == "Access Granted")
        {
            return true;
        }
        else
        {
            return false;   
        }
        
    }

    private void BindGrid()
    {
        using (var ds = new DataSet())
        {
            ds.ReadXml(Server.MapPath("/data/Servers.xml"));
            serverListTable.DataSource = ds;
            serverListTable.DataBind();

            //Set Heading Title
            switch (ds.Tables[0].Rows[0][3].ToString())
            {
                case "HXB30320":
                    lnkHeading.Text = "DEV 1";
                    break;
                case "HXC30320":
                    lnkHeading.Text = "ST 1";
                    break;
                case "HXD30320":
                    lnkHeading.Text = "UAT 1";
                    break;
                case "HXE30320":
                    lnkHeading.Text = "STAGING 1";
                    break;
                case "HXP30320":
                    lnkHeading.Text = "PROD 1";
                    break;
            }
        }
    }

    protected void btnCheckPassword_Click(object sender, EventArgs e)
    {
        if (AllowAccess())
        {
            btnGo.Visible = false;
            txtSecurity.Visible = false;
            lblElevatedPermissions.Text = "Access Granted";
            BindGrid();
        }
    }
}
