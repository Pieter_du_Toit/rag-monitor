﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class _Default : Page
{
    readonly DataSet ServerDataSet = new DataSet();

    private String server0Address;
    private String server1Address;
    private String server2Address;
    private String server3Address;
    private String server4Address;
    private String server5Address;
    private String server6Address;

    private String server0TestAddress;
    private String server1TestAddress;
    private String server2TestAddress;
    private String server3TestAddress;
    private String server4TestAddress;
    private String server5TestAddress;
    private String server6TestAddress;

    private String server0Name;
    private String server1Name;
    private String server2Name;
    private String server3Name;
    private String server4Name;
    private String server5Name;
    private String server6Name;
    
    private String server0LastTimeTested;
    private String server1LastTimeTested;
    private String server2LastTimeTested;
    private String server3LastTimeTested;
    private String server4LastTimeTested;
    private String server5LastTimeTested;
    private String server6LastTimeTested;
    private String serverDBConnectionsLastTimeTested;
    private String serverUSDCDocsLastTimeTested;
    private String serverJRulesLastTimeTested;
    private String serverDM5LastTimeTested;
    private String serverThunderheadLastTimeTested;
    private String serverEDILastTimeTested;
    private String serverQASLastTimeTested;

    private String serverDBConnectionsAddress;
    private String serverUSDCDocsAddress;
    private String serverJRulesAddress;
    private String serverDM5Address;
    private String serverThunderheadAddress;
    private String serverEDIAddress;
    private String serverEDITestAddress;
    private String serverQASAddress;

    private String thunderTestType;
    private String qasTestType;
    private String dbTestType;
    private String dm5TestType;

    private String serverOnePollTestResult;
    private String serverOneTaskTestResult;
    private String serverOneConnectionColor;
    private String serverTwoPollTestResult;
    private String serverTwoTaskTestResult;
    private String serverTwoConnectionColor; 
    private String serverThreePollTestResult;
    private String serverThreeTaskTestResult;
    private String serverThreeConnectionColor;
    private String serverFourPollTestResult;
    private String serverFourTaskTestResult;
    private String serverFourConnectionColor;
    private String serverFivePollTestResult;
    private String serverFiveTaskTestResult;
    private String serverFiveConnectionColor;
    private String serverSixPollTestResult;
    private String serverSixTaskTestResult;
    private String serverSixConnectionColor;
    private String serverSevenPollTestResult;
    private String serverSevenTaskTestResult;
    private String serverSevenConnectionColor;
    private String serverDBConnTestResult;
    private String serverDBConnectionColor;
    private String serverUSDCDocsTestResult;
    private String serverUSDCDocsConnectionColor;
    private String serverJRulesTestResult;
    private String serverJRulesConnectionColor;
    private String serverDM5TestResult;
    private String serverDM5ConnectionColor;
    private String serverThunderheadTestResult;
    private String serverThunderheadConnectionColor;
    private String serverEDITestResult;
    private String serverEDIConnectionColor;
    private String serverQASTestResult;
    private String serverQASConnectionColor;

    private bool IsServerOneActive;
    private bool IsServerTwoActive;
    private bool IsServerThreeActive;
    private bool IsServerFourActive;
    private bool IsServerFiveActive;
    private bool IsServerSixActive;
    private bool IsServerSevenActive;
    private bool IsServerDBConnectionsActive;
    private bool IsServerUSDCDocsActive;
    private bool IsServerJRulesActive;
    private bool IsServerDM5Active;
    private bool IsServerThunderheadActive;
    private bool IsServerEDIActive;
    private bool IsServerQASActive;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //Clear table
        ServerDataSet.Clear();
        //Load data into dataset
        LoadTestDataFromXMLFileIntoDataSet();
        //Populate variables from dataset
        PopulateVariablesReadFromDataSet();
        //Display the data on screen
        UpdateMainScreen();
        //Hide sections that are disabled
        HideDisabledSections();
    }
    
    
    private void LoadTestDataFromXMLFileIntoDataSet()
    {
        ServerDataSet.ReadXml(Server.MapPath("/data/Servers.xml"));
    }

    private void PopulateVariablesReadFromDataSet()
    {
        server0Name = ServerDataSet.Tables[0].Rows[0].ItemArray[3].ToString();
        server0Address = ServerDataSet.Tables[0].Rows[0].ItemArray[4].ToString();
        server0TestAddress = ServerDataSet.Tables[0].Rows[0].ItemArray[5].ToString();
        IsServerOneActive = ServerDataSet.Tables[0].Rows[0].ItemArray[10].ToString() == "1";
        serverOnePollTestResult = ServerDataSet.Tables[0].Rows[0].ItemArray[11].ToString();
        serverOneTaskTestResult = ServerDataSet.Tables[0].Rows[0].ItemArray[12].ToString();
        serverOneConnectionColor = ServerDataSet.Tables[0].Rows[0].ItemArray[13].ToString();
        server0LastTimeTested = ServerDataSet.Tables[0].Rows[0].ItemArray[7].ToString();

        server1Name = ServerDataSet.Tables[0].Rows[1].ItemArray[3].ToString();
        server1Address = ServerDataSet.Tables[0].Rows[1].ItemArray[4].ToString();
        server1TestAddress = ServerDataSet.Tables[0].Rows[1].ItemArray[5].ToString();
        IsServerTwoActive = ServerDataSet.Tables[0].Rows[1].ItemArray[10].ToString() == "1";
        serverTwoPollTestResult = ServerDataSet.Tables[0].Rows[1].ItemArray[11].ToString();
        serverTwoTaskTestResult = ServerDataSet.Tables[0].Rows[1].ItemArray[12].ToString();
        serverTwoConnectionColor = ServerDataSet.Tables[0].Rows[1].ItemArray[13].ToString();
        server1LastTimeTested = ServerDataSet.Tables[0].Rows[1].ItemArray[7].ToString();

        server2Name = ServerDataSet.Tables[0].Rows[2].ItemArray[3].ToString();
        server2Address = ServerDataSet.Tables[0].Rows[2].ItemArray[4].ToString();
        server2TestAddress = ServerDataSet.Tables[0].Rows[2].ItemArray[5].ToString();
        IsServerThreeActive = ServerDataSet.Tables[0].Rows[2].ItemArray[10].ToString() == "1";
        serverThreePollTestResult = ServerDataSet.Tables[0].Rows[2].ItemArray[11].ToString();
        serverThreeTaskTestResult = ServerDataSet.Tables[0].Rows[2].ItemArray[12].ToString();
        serverThreeConnectionColor = ServerDataSet.Tables[0].Rows[2].ItemArray[13].ToString();
        server2LastTimeTested = ServerDataSet.Tables[0].Rows[2].ItemArray[7].ToString();

        server3Name = ServerDataSet.Tables[0].Rows[3].ItemArray[3].ToString();
        server3Address = ServerDataSet.Tables[0].Rows[3].ItemArray[4].ToString();
        server3TestAddress = ServerDataSet.Tables[0].Rows[3].ItemArray[5].ToString();
        IsServerFourActive = ServerDataSet.Tables[0].Rows[3].ItemArray[10].ToString() == "1";
        serverFourPollTestResult = ServerDataSet.Tables[0].Rows[3].ItemArray[11].ToString();
        serverFourTaskTestResult = ServerDataSet.Tables[0].Rows[3].ItemArray[12].ToString();
        serverFourConnectionColor = ServerDataSet.Tables[0].Rows[3].ItemArray[13].ToString();
        server3LastTimeTested = ServerDataSet.Tables[0].Rows[3].ItemArray[7].ToString();

        server4Name = ServerDataSet.Tables[0].Rows[4].ItemArray[3].ToString();
        server4Address = ServerDataSet.Tables[0].Rows[4].ItemArray[4].ToString();
        server4TestAddress = ServerDataSet.Tables[0].Rows[4].ItemArray[5].ToString();
        IsServerFiveActive = ServerDataSet.Tables[0].Rows[4].ItemArray[10].ToString() == "1";
        serverFivePollTestResult = ServerDataSet.Tables[0].Rows[4].ItemArray[11].ToString();
        serverFiveTaskTestResult = ServerDataSet.Tables[0].Rows[4].ItemArray[12].ToString();
        serverFiveConnectionColor = ServerDataSet.Tables[0].Rows[4].ItemArray[13].ToString();
        server4LastTimeTested = ServerDataSet.Tables[0].Rows[4].ItemArray[7].ToString();

        server5Name = ServerDataSet.Tables[0].Rows[5].ItemArray[3].ToString();
        server5Address = ServerDataSet.Tables[0].Rows[5].ItemArray[4].ToString();
        server5TestAddress = ServerDataSet.Tables[0].Rows[5].ItemArray[5].ToString();
        IsServerSixActive = ServerDataSet.Tables[0].Rows[5].ItemArray[10].ToString() == "1";
        serverSixPollTestResult = ServerDataSet.Tables[0].Rows[5].ItemArray[11].ToString();
        serverSixTaskTestResult = ServerDataSet.Tables[0].Rows[5].ItemArray[12].ToString();
        serverSixConnectionColor = ServerDataSet.Tables[0].Rows[5].ItemArray[13].ToString();
        server5LastTimeTested = ServerDataSet.Tables[0].Rows[5].ItemArray[7].ToString();

        server6Name = ServerDataSet.Tables[0].Rows[6].ItemArray[3].ToString();
        server6Address = ServerDataSet.Tables[0].Rows[6].ItemArray[4].ToString();
        server6TestAddress = ServerDataSet.Tables[0].Rows[6].ItemArray[5].ToString();
        IsServerSevenActive = ServerDataSet.Tables[0].Rows[6].ItemArray[10].ToString() == "1";
        serverSevenPollTestResult = ServerDataSet.Tables[0].Rows[6].ItemArray[11].ToString();
        serverSevenTaskTestResult = ServerDataSet.Tables[0].Rows[6].ItemArray[12].ToString();
        serverSevenConnectionColor = ServerDataSet.Tables[0].Rows[6].ItemArray[13].ToString();
        server6LastTimeTested = ServerDataSet.Tables[0].Rows[6].ItemArray[7].ToString();

        serverDBConnectionsAddress = ServerDataSet.Tables[0].Rows[7].ItemArray[4].ToString();
        dbTestType = ServerDataSet.Tables[0].Rows[7].ItemArray[1].ToString();
        IsServerDBConnectionsActive = ServerDataSet.Tables[0].Rows[7].ItemArray[10].ToString() == "1";
        serverDBConnTestResult = ServerDataSet.Tables[0].Rows[7].ItemArray[11].ToString();
        serverDBConnectionColor = ServerDataSet.Tables[0].Rows[7].ItemArray[13].ToString();
        serverDBConnectionsLastTimeTested = ServerDataSet.Tables[0].Rows[7].ItemArray[7].ToString();
            
        serverEDIAddress = ServerDataSet.Tables[0].Rows[8].ItemArray[4].ToString();
        serverEDITestAddress = ServerDataSet.Tables[0].Rows[8].ItemArray[5].ToString();
        IsServerEDIActive = ServerDataSet.Tables[0].Rows[8].ItemArray[10].ToString() == "1";
        serverEDITestResult = ServerDataSet.Tables[0].Rows[8].ItemArray[11].ToString();
        serverEDIConnectionColor = ServerDataSet.Tables[0].Rows[8].ItemArray[13].ToString();
        serverEDILastTimeTested = ServerDataSet.Tables[0].Rows[8].ItemArray[7].ToString();

        serverDM5Address = ServerDataSet.Tables[0].Rows[9].ItemArray[4].ToString();
        dm5TestType = ServerDataSet.Tables[0].Rows[9].ItemArray[1].ToString();
        IsServerDM5Active = ServerDataSet.Tables[0].Rows[9].ItemArray[10].ToString() == "1";
        serverDM5TestResult = ServerDataSet.Tables[0].Rows[9].ItemArray[11].ToString();
        serverDM5ConnectionColor = ServerDataSet.Tables[0].Rows[9].ItemArray[13].ToString();
        serverDM5LastTimeTested = ServerDataSet.Tables[0].Rows[9].ItemArray[7].ToString();

        serverThunderheadAddress = ServerDataSet.Tables[0].Rows[10].ItemArray[4].ToString();
        thunderTestType = ServerDataSet.Tables[0].Rows[10].ItemArray[1].ToString();
        IsServerThunderheadActive = ServerDataSet.Tables[0].Rows[10].ItemArray[10].ToString() == "1";
        serverThunderheadTestResult = ServerDataSet.Tables[0].Rows[10].ItemArray[11].ToString();
        serverThunderheadConnectionColor = ServerDataSet.Tables[0].Rows[10].ItemArray[13].ToString();
        serverThunderheadLastTimeTested = ServerDataSet.Tables[0].Rows[10].ItemArray[7].ToString();

        serverUSDCDocsAddress = ServerDataSet.Tables[0].Rows[11].ItemArray[4].ToString();
        IsServerUSDCDocsActive = ServerDataSet.Tables[0].Rows[11].ItemArray[10].ToString() == "1";
        serverUSDCDocsTestResult = ServerDataSet.Tables[0].Rows[11].ItemArray[11].ToString();
        serverUSDCDocsConnectionColor = ServerDataSet.Tables[0].Rows[11].ItemArray[13].ToString();
        serverUSDCDocsLastTimeTested = ServerDataSet.Tables[0].Rows[11].ItemArray[7].ToString();

        serverJRulesAddress = ServerDataSet.Tables[0].Rows[12].ItemArray[4].ToString();
        IsServerJRulesActive = ServerDataSet.Tables[0].Rows[12].ItemArray[10].ToString() == "1";
        serverJRulesTestResult = ServerDataSet.Tables[0].Rows[12].ItemArray[11].ToString();
        serverJRulesConnectionColor = ServerDataSet.Tables[0].Rows[12].ItemArray[13].ToString();
        serverJRulesLastTimeTested = ServerDataSet.Tables[0].Rows[12].ItemArray[7].ToString();

        serverQASAddress = ServerDataSet.Tables[0].Rows[13].ItemArray[4].ToString();
        qasTestType = ServerDataSet.Tables[0].Rows[13].ItemArray[1].ToString();
        IsServerQASActive = ServerDataSet.Tables[0].Rows[13].ItemArray[10].ToString() == "1";
        serverQASTestResult = ServerDataSet.Tables[0].Rows[13].ItemArray[11].ToString();
        serverQASConnectionColor = ServerDataSet.Tables[0].Rows[13].ItemArray[13].ToString();
        serverQASLastTimeTested = ServerDataSet.Tables[0].Rows[13].ItemArray[7].ToString();

        // If for some reason there is not a valid colour set, then default to red
        serverOneConnectionColor = setDefaultRedIfNotValidColor(serverOneConnectionColor);
        serverTwoConnectionColor = setDefaultRedIfNotValidColor(serverTwoConnectionColor);
        serverThreeConnectionColor = setDefaultRedIfNotValidColor(serverThreeConnectionColor);
        serverFourConnectionColor = setDefaultRedIfNotValidColor(serverFourConnectionColor);
        serverFiveConnectionColor = setDefaultRedIfNotValidColor(serverFiveConnectionColor);
        serverSixConnectionColor = setDefaultRedIfNotValidColor(serverSixConnectionColor);
        serverSevenConnectionColor = setDefaultRedIfNotValidColor(serverSevenConnectionColor);
    }

    /// <summary>
    /// Returns red if input string is not valid colour.
    /// </summary>
    /// <param name="connectionColor">Input colour string</param>
    /// <returns>Returns red if input string is not valid colour</returns>
    private string setDefaultRedIfNotValidColor(string connectionColor)
    {
        if (connectionColor != "red" && connectionColor != "amber" &&
                connectionColor != "green" && connectionColor != "blue")
        {
            connectionColor = "red";
        }
        return connectionColor;
    }

    private void UpdateMainScreen()
    {
        switch (server0Name)
        {
            case "HXB30320":
                lnkHeading.Text = "DEV 1";
                break;
            case "HXC30320":
                lnkHeading.Text = "ST 1";
                break;
            case "HXD30320":
                lnkHeading.Text = "UAT 1";
                break;
            case "HXE30320":
                lnkHeading.Text = "STAGING 1";
                break;
            case "HXP30320":
                lnkHeading.Text = "PROD 1";
                break;
        }

        lnkServerName0.Text = server0Name;
        ServerName1.Attributes["class"] = serverOneConnectionColor + "Corner";
        divPoll1.Attributes["class"] = "divPoll " + serverOneConnectionColor + "Border";
        divTask1.Attributes["class"] = "divTask " + serverOneConnectionColor + "Border";
        divPoll1.InnerHtml = serverOnePollTestResult;
        divTask1.InnerHtml = serverOneTaskTestResult;
        ServerOneTime.Text = ConvertToTimeFormat(server0LastTimeTested);
        
        lnkServerName1.Text = server1Name;
        ServerName2.Attributes["class"] = serverTwoConnectionColor + "Corner";
        divPoll2.Attributes["class"] = "divPoll " + serverTwoConnectionColor + "Border";
        divTask2.Attributes["class"] = "divTask " + serverTwoConnectionColor + "Border";
        divPoll2.InnerHtml = serverTwoPollTestResult;
        divTask2.InnerHtml = serverTwoTaskTestResult;
        ServerTwoTime.Text = ConvertToTimeFormat(server1LastTimeTested);

        lnkServerName2.Text = server2Name;
        ServerName3.Attributes["class"] = serverThreeConnectionColor + "Corner";
        divPoll3.Attributes["class"] = "divPoll " + serverThreeConnectionColor + "Border";
        divTask3.Attributes["class"] = "divTask " + serverThreeConnectionColor + "Border";
        divPoll3.InnerHtml = serverThreePollTestResult;
        divTask3.InnerHtml = serverThreeTaskTestResult;
        ServerThreeTime.Text = ConvertToTimeFormat(server2LastTimeTested);

        lnkServerName3.Text = server3Name;
        ServerName4.Attributes["class"] = serverFourConnectionColor + "Corner";
        divPoll4.Attributes["class"] = "divPoll " + serverFourConnectionColor + "Border";
        divTask4.Attributes["class"] = "divTask " + serverFourConnectionColor + "Border";
        divPoll4.InnerHtml = serverFourPollTestResult;
        divTask4.InnerHtml = serverFourTaskTestResult;
        ServerFourTime.Text = ConvertToTimeFormat(server3LastTimeTested);

        lnkServerName4.Text = server4Name;
        ServerName5.Attributes["class"] = serverFiveConnectionColor + "Corner";
        divPoll5.Attributes["class"] = "divPoll " + serverFiveConnectionColor + "Border";
        divTask5.Attributes["class"] = "divTask " + serverFiveConnectionColor + "Border";
        divPoll5.InnerHtml = serverFivePollTestResult;
        divTask5.InnerHtml = serverFiveTaskTestResult;
        ServerFiveTime.Text = ConvertToTimeFormat(server4LastTimeTested);

        lnkServerName5.Text = server5Name;
        ServerName6.Attributes["class"] = serverSixConnectionColor + "Corner";
        divPoll6.Attributes["class"] = "divPoll " + serverSixConnectionColor + "Border";
        divTask6.Attributes["class"] = "divTask " + serverSixConnectionColor + "Border";
        divPoll6.InnerHtml = serverSixPollTestResult;
        divTask6.InnerHtml = serverSixTaskTestResult;
        ServerSixTime.Text = ConvertToTimeFormat(server5LastTimeTested);   

        lnkServerName6.Text = server6Name;
        ServerName7.Attributes["class"] = serverSevenConnectionColor + "Corner";
        divPoll7.Attributes["class"] = "divPoll " + serverSevenConnectionColor + "Border";
        divTask7.Attributes["class"] = "divTask " + serverSevenConnectionColor + "Border";
        divPoll7.InnerHtml = serverSevenPollTestResult;
        divTask7.InnerHtml = serverSevenTaskTestResult;
        ServerSevenTime.Text = ConvertToTimeFormat(server6LastTimeTested);   
 
        //USDC
        switch (serverUSDCDocsConnectionColor)
        {
            case "green":
                divUSDCHead.Attributes["class"] = "greenCorner";
                divUSDCBlock.Attributes["class"] = "divBorder greenBorder";
                break;
            case "amber":
                divUSDCHead.Attributes["class"] = "amberCorner";
                divUSDCBlock.Attributes["class"] = "divBorder amberBorder";
                break;
            case "blue":
                divUSDCHead.Attributes["class"] = "blueCorner";
                divUSDCBlock.Attributes["class"] = "divBorder blueBorder";
                break;
            default:
                divUSDCHead.Attributes["class"] = "redCorner";
                divUSDCBlock.Attributes["class"] = "divBorder redBorder";
                break;
        }
        divUSDCBlock.InnerHtml = serverUSDCDocsTestResult;
        USDCTime.Text = ConvertToTimeFormat(serverUSDCDocsLastTimeTested);
        
        //JRules
        switch (serverJRulesConnectionColor)
        {
            case "amber":
                divJRulesHead.Attributes["class"] = "amberCorner";
                divJRulesBlock.Attributes["class"] = "divBorderThin amberBorder";
                break;
            case "green":
                divJRulesHead.Attributes["class"] = "greenCorner";
                divJRulesBlock.Attributes["class"] = "divBorderThin greenBorder";
                break;
            case "blue":
                divJRulesHead.Attributes["class"] = "blueCorner";
                divJRulesBlock.Attributes["class"] = "divBorderThin blueBorder";
                break;
            default:
                divJRulesHead.Attributes["class"] = "redCorner";
                divJRulesBlock.Attributes["class"] = "divBorderThin redBorder";
                break;
        }
        divJRulesBlock.InnerHtml = serverJRulesTestResult;
        JRulesTime.Text = ConvertToTimeFormat(serverJRulesLastTimeTested);
            
        // DB Conn
        switch (serverDBConnectionColor)
        {
            case "green":
                divDBConnHead.Attributes["class"] = "greenCorner";
                divDBConnBlock.Attributes["class"] = "divBorderThin greenBorder";
                break;
            case "amber":
                divDBConnHead.Attributes["class"] = "amberCorner";
                divDBConnBlock.Attributes["class"] = "divBorderThin amberBorder";
                break;
            case "blue":
                divDBConnHead.Attributes["class"] = "blueCorner";
                divDBConnBlock.Attributes["class"] = "divBorderThin blueBorder";
                break;
            default:
                divDBConnHead.Attributes["class"] = "redCorner";
                divDBConnBlock.Attributes["class"] = "divBorderThin redBorder";
                break;
        }
        divDBConnBlock.InnerHtml = serverDBConnTestResult;
        DBConnTime.Text = ConvertToTimeFormat(serverDBConnectionsLastTimeTested);

        //Thunderhead
        switch (serverThunderheadConnectionColor)
        {
            case "blue":
                divThunderHead.Attributes["class"] = "blueCorner";
                divThunderBlock.Attributes["class"] = "divBorderThin blueBorder";
                break;
            case "green":
                divThunderHead.Attributes["class"] = "greenCorner";
                divThunderBlock.Attributes["class"] = "divBorderThin greenBorder";
                break;
            case "amber":
                divThunderHead.Attributes["class"] = "amberCorner";
                divThunderBlock.Attributes["class"] = "divBorderThin amberBorder";
                break;
            default:
                divThunderHead.Attributes["class"] = "redCorner";
                divThunderBlock.Attributes["class"] = "divBorderThin redBorder";
                break;
        }
        divThunderBlock.InnerHtml = serverThunderheadTestResult;
        ThunderTime.Text = ConvertToTimeFormat(serverThunderheadLastTimeTested);

        //DM5
        switch (serverDM5ConnectionColor)
        {
            case "blue":
                divDM5Head.Attributes["class"] = "blueCorner";
                divDM5Block.Attributes["class"] = "divBorderThin blueBorder";
                break;
            case "green":
                divDM5Head.Attributes["class"] = "greenCorner";
                divDM5Block.Attributes["class"] = "divBorderThin greenBorder";
                break;
            case "amber":
                divDM5Head.Attributes["class"] = "amberCorner";
                divDM5Block.Attributes["class"] = "divBorderThin amberBorder";
                break;
            default:
                divDM5Head.Attributes["class"] = "redCorner";
                divDM5Block.Attributes["class"] = "divBorderThin redBorder";
                break;
        }
        divDM5Block.InnerHtml = serverDM5TestResult;
        DM5Time.Text = ConvertToTimeFormat(serverDM5LastTimeTested);

        //EDI
        switch (serverEDIConnectionColor)
        {
            case "blue":
                divEDIHead.Attributes["class"] = "blueCorner";
                divEDIBlock.Attributes["class"] = "divBorderThin blueBorder";
                break;
            case "green":
                divEDIHead.Attributes["class"] = "greenCorner";
                divEDIBlock.Attributes["class"] = "divBorderThin greenBorder";
                break;
            case "amber":
                divEDIHead.Attributes["class"] = "amberCorner";
                divEDIBlock.Attributes["class"] = "divBorderThin amberBorder";
                break;
            default:
                divEDIHead.Attributes["class"] = "redCorner";
                divEDIBlock.Attributes["class"] = "divBorderThin redBorder";
                break;
        }
        divEDIBlock.InnerHtml = serverEDITestResult;   
        EDITime.Text = ConvertToTimeFormat(serverEDILastTimeTested);
            
        //QAS
        switch (serverQASConnectionColor)
        {
            case "blue":
                divQASHead.Attributes["class"] = "blueCorner";
                divQASBlock.Attributes["class"] = "divBorderThin blueBorder";
                break;
            case "green":
                divQASHead.Attributes["class"] = "greenCorner";
                divQASBlock.Attributes["class"] = "divBorderThin greenBorder";
                break;
            case "amber":
                divQASHead.Attributes["class"] = "amberCorner";
                divQASBlock.Attributes["class"] = "divBorderThin amberBorder";
                break;
            default:
                divQASHead.Attributes["class"] = "redCorner";
                divQASBlock.Attributes["class"] = "divBorderThin redBorder";
                break;
        }
        divQASBlock.InnerHtml = serverQASTestResult;
        QASTime.Text = ConvertToTimeFormat(serverQASLastTimeTested);
    }

    private string ConvertToTimeFormat(string dateAndTimeNumber)
    {
        var HHmmSS = dateAndTimeNumber.Substring(8, 6);
        var HH = HHmmSS.Substring(0, 2);
        var mm = HHmmSS.Substring(2, 2);
        var SS = HHmmSS.Substring(4, 2);
        var theTime = HH + ":" + mm + ":" + SS;
        return theTime;
    }
    
    protected void PageTimer_Tick(object sender, EventArgs e)
    {
        LoadTestDataFromXMLFileIntoDataSet();
        PopulateVariablesReadFromDataSet();
        UpdateMainScreen();
    }

    private static string CallService(NameValueCollection parameterCollection, string server)
    {
        string returnString;
        var urlString = "http://" + server + "/Services.aspx?";

        using (var client = new WebClient())
        {
            var responsebytes = client.UploadValues(urlString, "POST", parameterCollection);
            returnString = Encoding.UTF8.GetString(responsebytes);
        }
        return returnString;
    }

    #region IIS Tests

        private void TestIISServer(int testID, string serverAddress, string serverTestAddress, string serverName)
        {
            var color = GetIISServerConnectionStatus(serverAddress, serverTestAddress);
            var poll = TestPolling(serverAddress, serverName);
            var task = TestTaskCounter(serverAddress);
            UpdateXMLFileWithATestResult(testID, poll, task, color);
        }

        private string GetIISServerConnectionStatus(string serverAddress, string testAddress)
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckServerConnection,-A" + testAddress);
                var result = CallService(reqparm, serverAddress);
                return result;
            }
            catch
            {
                return "red";
            }
        }

        private static string TestPolling(string serverAddress, string serverName)
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckLastPollingTask,-A" + serverName);
                var result = CallService(reqparm, serverAddress);
                return result.Length > 0 ? result : "<div style='text-align:center'><b>Polling - Last Completed Tasks</b></div><div style='text-align:center'>Polling service returned no value!</div>";
            }
            catch (WebException ex)
            {
                return ex.Status != WebExceptionStatus.Success ? "<div style='text-align:center'><b>Polling - Last Completed Taks</b></div><div style='color: red; text-align:center;'>Polling service had a total failure!</div>" : "<div style='text-align:center'><b>Polling - Completed Tasks</b></div><div style='color: red; text-align:center;'>Polling service failure!</div>";
            }
            
        }

        private static string TestTaskCounter(string serverAddress)
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("Counters", "Y");
                var result = CallService(reqparm, serverAddress);
                return result.Length > 0 ? result : "<div  style='text-align:center'><b>Tasks - Current Totals</b></div><div style='text-align:center'>Task Counter service returned no values!</div>";
            }
            catch (WebException ex)
            {
                return ex.Status != WebExceptionStatus.Success ? "<div style='text-align:center'><b>Tasks - Current Totals</b></div><div style='color: red; text-align:center'>Task service had a total failure!</div>" : "<div style='color: red; text-align:center'>Task service failure!</div>";
            }
        }
    
    #endregion

    #region Non IIS Tests

        private void TestUSDCDocuments()
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckUSDCDocuments");
                var result = CallService(reqparm, serverUSDCDocsAddress);
                UpdateXMLFileWithATestResult("11", result, result.Contains("red") ? "amber" : "green");
            }
            catch
            {
                UpdateXMLFileWithATestResult("11", "USDC Document service had a total failure", "red");
            }
        }

        private void TestJRules()
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckJRules");
                var result = CallService(reqparm, serverJRulesAddress);
                if (result == "green"){UpdateXMLFileWithATestResult("12", "JRules service tested fine", "green");}
                else if (result == "amber"){UpdateXMLFileWithATestResult("12", "JRules service has a problem", "amber");}
                else if (result == "red"){UpdateXMLFileWithATestResult("12", "JRules service failed", "red");}
                else if (result == ""){UpdateXMLFileWithATestResult("12", "JRules service returned no value", "blue");}
                else{ UpdateXMLFileWithATestResult("12", "Did not recognize the result", "amber");}
            }
            catch
            {
                UpdateXMLFileWithATestResult("12", "JRules service had a total failure", "red");
            }
        }

        private void TestDBConnections()
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckDatabaseConnection,-A" + dbTestType);
                var result = CallService(reqparm, serverDBConnectionsAddress);
                UpdateXMLFileWithATestResult("7", result, result.Contains("red") ? "amber" : "green");
            }
            catch
            {
                UpdateXMLFileWithATestResult("7", "Database service had a total failure", "red");
            }
        }

        private void TestThunderhead()
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckThunderhead,-A" + thunderTestType);
                
                var result = CallService(reqparm, serverThunderheadAddress);
                if(result == "green"){UpdateXMLFileWithATestResult("10", "Thunderhead service tested fine", "green");}
                else if (result == "amber") { UpdateXMLFileWithATestResult("10", "Thunderhead service had a problem", "amber"); }
                else if (result == "blue") { UpdateXMLFileWithATestResult("10", "Thunderhead service returned no value", "blue"); }
                else if (result == "red") { UpdateXMLFileWithATestResult("10", "Thunderhead service failed", "red"); }
                else { UpdateXMLFileWithATestResult("10", "Did not recognize the result", "amber"); }
                
            }
            catch
            {
                UpdateXMLFileWithATestResult("10","Thunderhead service had a total failure", "red");
            }
        }

        private void TestDM5()
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckDM5");
                if (dm5TestType == "DM5DEV")
                {
                    UpdateXMLFileWithATestResult("9", "DM5 is not available in dev", "blue");
                }
                else
                {
                    var result = CallService(reqparm, serverDM5Address);
                    if (result == "green"){UpdateXMLFileWithATestResult("9", "DM5 service tested fine", "green");}
                    else if (result == "amber"){UpdateXMLFileWithATestResult("9", "DM5 service had a problem", "amber");}
                    else if (result == "red"){UpdateXMLFileWithATestResult("9", "DM5 service failed", "red");}
                    else if (result == ""){UpdateXMLFileWithATestResult("9", "DM5 service returned no value", "blue");}
                    else{UpdateXMLFileWithATestResult("9", "Did not recognize the result", "amber");}
                }
            }
            catch
            {
                UpdateXMLFileWithATestResult("9","DM5 service had a total failure", "red");
            }
        }

        private void TestEDI()
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckEDI,-A" + serverEDITestAddress);
                
                var result = CallService(reqparm, serverEDIAddress);
                if (result == "green"){UpdateXMLFileWithATestResult("8", "EDI service tested fine", "green");}
                else if (result == "amber"){UpdateXMLFileWithATestResult("8", "EDI service had a problem", "amber");}
                else if (result == "red"){UpdateXMLFileWithATestResult("8", "EDI service failed", "red");}
                else if (result == ""){UpdateXMLFileWithATestResult("8", "EDI service returned no value", "blue");}
                else{UpdateXMLFileWithATestResult("8", "Did not recognize the result", "amber");}
            }
            catch
            {
                UpdateXMLFileWithATestResult("8", "EDI service had a total failure", "red");
            }
        }

        private void TestQAS()
        {
            try
            {
                var reqparm = new NameValueCollection();
                reqparm.Clear();
                reqparm.Add("PRGNAME", "DashBoardService");
                reqparm.Add("ARGUMENTS", "-ACheckQAS,-A" + qasTestType);
                var result = CallService(reqparm, serverQASAddress);

                if (result == "green"){UpdateXMLFileWithATestResult("13", "QAS service tested fine", "green");}
                else if (result == "amber"){UpdateXMLFileWithATestResult("13", "QAS service had a problem", "amber");}
                else if (result == "red"){UpdateXMLFileWithATestResult("13", "QAS service failed", "red");}
                else if (result == ""){UpdateXMLFileWithATestResult("13", "QAS service returned no value", "blue");}
                else{UpdateXMLFileWithATestResult("13", "Did not recognize the result", "amber");}
            }
            catch
            {
                UpdateXMLFileWithATestResult("13", "QAS service had a total failure", "red");
            }
        }

    #endregion

    #region Updating XML

        private void UpdateXMLFileWithATestResult(int testID, string result1, string result2, string color)
    {
        // Load the xml file
        var doc = new XmlDocument();

        doc.Load(Server.MapPath("/data/Servers.xml"));

        // Locate the xml tags which we need to update
        var LastTestRunTime = doc.SelectSingleNode("Servers//Server[TestID='" + testID + "']/LastTestRun");
        var TestResult1 = doc.SelectSingleNode("Servers//Server[TestID='" + testID + "']/TestResult1");
        var TestResult2 = doc.SelectSingleNode("Servers//Server[TestID='" + testID + "']/TestResult2");
        var ConnectionColor = doc.SelectSingleNode("Servers//Server[TestID='" + testID + "']/ConnectionColor");
            
        // Save the results to the located xml tags
        LastTestRunTime.InnerText = DateTime.Now.ToString("yyyyMMddHHmmss");
        TestResult1.InnerText = result1;
        TestResult2.InnerText = result2;
        ConnectionColor.InnerText = color;

        try
        {
            doc.Save(Server.MapPath("/data/Servers.xml"));
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect("/ErrorPages/AccessToServerConfigXML.aspx", true);
        }
        catch (AccessViolationException)
        {
            Response.Redirect("/ErrorPages/AccessToServerConfigXML.aspx", true);
        }

    }

        private void UpdateXMLFileWithATestResult(string testID, string resultValue, string color)
    {
        // Load the xml file
        var doc = new XmlDocument();
        doc.Load(Server.MapPath("/data/Servers.xml"));

        // Locate the xml tags which we need to update
        var LastTestRunTime = doc.SelectSingleNode("Servers//Server[TestID='" + testID + "']/LastTestRun");
        var TestResult1 = doc.SelectSingleNode("Servers//Server[TestID='" + testID + "']/TestResult1");
        var ConnectionColor = doc.SelectSingleNode("Servers//Server[TestID='" + testID + "']/ConnectionColor");

        // Save the results to the located xml tags
        LastTestRunTime.InnerText = DateTime.Now.ToString("yyyyMMddHHmmss");
        TestResult1.InnerText = resultValue;
        ConnectionColor.InnerText = color;

        try
        {
            doc.Save(Server.MapPath("/data/Servers.xml"));
        }
        catch (UnauthorizedAccessException)
        {
            Response.Redirect("/ErrorPages/AccessToServerConfigXML.aspx", true);
        }
        catch (AccessViolationException)
        {
            Response.Redirect("/ErrorPages/AccessToServerConfigXML.aspx", true);
        }

        GC.Collect();
        GC.WaitForPendingFinalizers();
    }

    #endregion

    #region Button Clicks

        protected void btnTestServer0_Click(object sender, EventArgs e)
        {
            TestIISServer(0, server0Address, server0TestAddress, server0Name);
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestServer1_Click(object sender, EventArgs e)
        {
            TestIISServer(1, server1Address, server1TestAddress, server1Name);
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestServer2_Click(object sender, EventArgs e)
        {
            TestIISServer(2, server2Address, server2TestAddress, server2Name);
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestServer3_Click(object sender, EventArgs e)
        {
            TestIISServer(3, server3Address, server3TestAddress, server3Name);
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestServer4_Click(object sender, EventArgs e)
        {
            TestIISServer(4, server4Address, server4TestAddress, server4Name);
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestServer5_Click(object sender, EventArgs e)
        {
            TestIISServer(5, server5Address, server5TestAddress, server5Name);
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestServer6_Click(object sender, EventArgs e)
        {
            TestIISServer(6, server6Address, server6TestAddress, server6Name);
            Response.Redirect(Request.RawUrl);
        }

        protected void btnTestIIS_Click(object sender, EventArgs e)
        {
            if (IsServerOneActive) { TestIISServer(0, server0Address, server0TestAddress, server0Name); }
            
            if (IsServerTwoActive) { TestIISServer(1, server1Address, server1TestAddress, server1Name); }
            
            if (IsServerThreeActive) { TestIISServer(2, server2Address, server2TestAddress, server2Name); }
            
            if (IsServerFourActive) { TestIISServer(3, server3Address, server3TestAddress, server3Name); }
            
            if (IsServerFiveActive) { TestIISServer(4, server4Address, server4TestAddress, server4Name); }
            
            if (IsServerSixActive) { TestIISServer(5, server5Address, server5TestAddress, server5Name); }
            
            if (IsServerSevenActive) { TestIISServer(6, server6Address, server6TestAddress, server6Name); }

            //Reload page to reflect changes in displayed content
            Response.Redirect(Request.RawUrl);
        }

        protected void btnTestDatabaseConnections_Click(object sender, EventArgs e)
        {
            TestDBConnections();
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestJRules_Click(object sender, EventArgs e)
        {
            TestJRules();
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestUsdcDocuments_Click(object sender, EventArgs e)
        {
            TestUSDCDocuments();
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestEDI_Click(object sender, EventArgs e)
        {
            TestEDI();
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestDM5_Click(object sender, EventArgs e)
        {
            TestDM5();
            Response.Redirect(Request.RawUrl);
        }
        protected void btnTestThunderhead_Click(object sender, EventArgs e)
        {
            TestThunderhead();
            Response.Redirect(Request.RawUrl);
        }
        protected void btnQAS_Click(object sender, EventArgs e)
        {
            TestQAS();
            Response.Redirect(Request.RawUrl);
        }

    #endregion

    #region Hiding

        private void HideDisabledSections()
        {
            if (!IsServerOneActive)
            {
                HideTheseDivs(new[] { "divTask1", "divPoll1", "ServerName1" });
            }
            if (!IsServerTwoActive)
            {
                HideTheseDivs(new[] { "divTask2", "divPoll2", "ServerName2" });
            }
            if (!IsServerThreeActive)
            {
                HideTheseDivs(new[] { "divTask3", "divPoll3", "ServerName3" });
            }
            if (!IsServerFourActive)
            {
                HideTheseDivs(new[] { "divTask4", "divPoll4", "ServerName4" });
            }
            if (!IsServerFiveActive)
            {
                HideTheseDivs(new[] { "divTask5", "divPoll5", "ServerName5" });
            }
            if (!IsServerSixActive)
            {
                HideTheseDivs(new[] { "divTask6", "divPoll6", "ServerName6" });
            }
            if (!IsServerSevenActive)
            {
                HideTheseDivs(new[] { "divTask7", "divPoll7", "ServerName7" });
            }
            if (!IsServerDBConnectionsActive)
            {
                HideTheseDivs(new[] { "divDBConnBlock", "divDBConnHead"});
            }
            if (!IsServerJRulesActive)
            {
                HideTheseDivs(new[] { "divJRulesBlock", "divJRulesHead" });
            }
            if (!IsServerUSDCDocsActive)
            {
                HideTheseDivs(new[] { "divUSDCBlock", "divUSDCHead" });
            }
            if (!IsServerEDIActive)
            {
                HideTheseDivs(new[] { "divEDIBlock", "divEDIHead" });
            }
            if (!IsServerDM5Active)
            {
                HideTheseDivs(new[] { "divDM5Block", "divDM5Head", "divSpaceAboveDM5" });
            }
            if (!IsServerThunderheadActive)
            {
                HideTheseDivs(new[] { "divThunderBlock", "divThunderHead", "divSpaceAboveThunder" });
            }
            if (!IsServerQASActive)
            {
                HideTheseDivs(new[] { "divQASBlock", "divQASHead", "divSpaceAboveQAS" });
            }
        }

        private void HideTheseDivs(IEnumerable<string> DivsToHide)
        {
            var contentPlaceHolder = (ContentPlaceHolder) Master.FindControl("MainContent");


            foreach (var divControl in DivsToHide.Select(contentPlaceHolder.FindControl).Where(divControl => divControl != null))
            {
                divControl.Visible = false;
            }
        }

    #endregion

    
}