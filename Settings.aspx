﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Maintenance" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <table runat="server" width="100%" style="background-color: #3DA0D9">
                <tr>
                    <td style="text-align: center; width: 85%">
                        <asp:LinkButton ID="lnkHeading" runat="server" CssClass="Main-HeaderWhite" OnClientClick="window.location.href='default.aspx'; return false;"></asp:LinkButton>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSecurity" BorderStyle="none" onkeydown="return (event.keyCode!=13);" Width="100px"></asp:TextBox>
                        <asp:Label runat="server" ID="lblElevatedPermissions" Text="" ForeColor="white"></asp:Label>
                        <asp:Button runat="server" ID="btnGo" BorderStyle="none" ForeColor="white" Text=">" BackColor="#3DA0D9" OnClick="btnCheckPassword_Click"></asp:Button>
                    </td>
                </tr>
            </table>
    <div id="serverslist">
        <asp:GridView ID="serverListTable" runat="server" AutoGenerateColumns="false" DataKeyNames="testID"
            OnRowCommand="serverListTable_RowCommand"
            OnRowEditing="serverListTable_OnRowEditing"
            OnRowCancelingEdit="serverListTable_RowCancelingEdit" 
            OnRowUpdating="serverListTable_RowUpdating"
            OnRowDataBound="serverListTable_RowDataBound"
            CellPadding="7" GridLines="Horizontal">
            <Columns>
                <asp:CommandField ShowEditButton="true" ShowCancelButton="true">
                    <ControlStyle CssClass="button-link"  />
                    <HeaderStyle CssClass="gvHeaderCenter"></HeaderStyle>
                </asp:CommandField>

                <asp:TemplateField HeaderText="Name">
                    <HeaderStyle Width="15%" CssClass="gvHeaderCenter" />
                    <ItemStyle Width="15%" CssClass="gvItemCenter" />
                    <ItemTemplate>
                        <asp:Label runat="server" Font-size="20px"><%# Eval("serverName")%></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="txtServerName" Font-size="20px" Text='<%# Eval("serverName")%>' MaxLength="15" Width="170px" />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Server IP">
                <HeaderStyle Width="15%" CssClass="gvHeaderCenter" />
                <ItemStyle Width="15%" CssClass="gvItemCenter"/>
                    <ItemTemplate>
                        <asp:Label runat="server" Font-size="20px"><%# Eval("serverIp")%></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="txtServerIP" Font-size="20px" Text='<%# Eval("serverIp")%>' MaxLength="15" Width="150px" />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Test IP">
                <HeaderStyle Width="15%" CssClass="gvHeaderCenter" />
                <ItemStyle Width="15%" CssClass="gvItemCenter"/>
                    <ItemTemplate>
                        <asp:Label runat="server" Font-size="20px"><%# Eval("TestIp")%></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox runat="server" ID="txtTestIP" Font-size="20px" Text='<%# Eval("TestIp")%>' MaxLength="15" Width="150px"/>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type">
                    <HeaderStyle Width="15%" CssClass="gvHeaderCenter" />
                        <ItemStyle Width="15%" CssClass="gvItemCenter"/>
                    <ItemTemplate>
                        <asp:Label runat="server" Font-size="20px"><%# Eval("TestType")%></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlTestType" runat="server" DataTextField='<%# Eval("[TestType]")%>' Font-Size="20px" Width="140px" SelectedValue='<%# Eval("[TestType]") %>'>
                            <asp:ListItem Text ="" Value = "" ></asp:ListItem>
                            <asp:ListItem Text ="IIS" Value = "IIS" ></asp:ListItem>
                            <asp:ListItem Text ="DBDEV" Value = "DBDEV" ></asp:ListItem>
                            <asp:ListItem Text ="DB" Value = "DB" ></asp:ListItem>
                            <asp:ListItem Text ="EDI" Value = "EDI" ></asp:ListItem>
                            <asp:ListItem Text ="DM5DEV" Value = "DM5DEV" ></asp:ListItem>
                            <asp:ListItem Text ="DM5" Value = "DM5" ></asp:ListItem>
                            <asp:ListItem Text ="ThunderDEV" Value = "ThunderDEV" ></asp:ListItem>
                            <asp:ListItem Text ="ThunderST" Value = "ThunderST" ></asp:ListItem>
                            <asp:ListItem Text ="ThunderUAT" Value = "ThunderUAT" ></asp:ListItem>
                            <asp:ListItem Text ="ThunderSTAG" Value = "ThunderSTAG" ></asp:ListItem>
                            <asp:ListItem Text ="ThunderPROD" Value = "ThunderPROD" ></asp:ListItem>
                            <asp:ListItem Text ="USDC Docs" Value = "USDC Docs" ></asp:ListItem>
                            <asp:ListItem Text ="JRules" Value = "JRules" ></asp:ListItem>
                            <asp:ListItem Text ="QASNONPROD" Value = "QASNONPROD" ></asp:ListItem>
                            <asp:ListItem Text ="QASPROD" Value = "QASPROD" ></asp:ListItem>
                            </asp:DropDownList>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <HeaderStyle Width="30%" CssClass="gvHeaderCenter"/>
                    <ItemStyle Width="30%" CssClass="gvItemCenter"/>
                        <ItemTemplate>
                            <asp:Label runat="server" Font-size="20px"><%# Eval("testDescription")%></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtTestDescription" Font-size="20px" Text='<%# Eval("testDescription")%>' MaxLength="100" Width="280px" />
                        </EditItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Interval">
                     <HeaderStyle Width="6%" CssClass="gvHeaderCenter"/>
                     <ItemStyle Width="6%" CssClass="gvItemCenter" />
                      <ItemTemplate>
                        <asp:Label runat="server" Font-size="20px"><%# Eval("TestInterval")%></asp:Label>
                      </ItemTemplate>
                      <EditItemTemplate>
                          <asp:DropDownList ID="ddlTestInterval" runat="server" DataTextField='<%# Eval("[TestInterval]")%>' Font-Size="20px" Width="100px" SelectedValue='<%# Eval("[TestInterval]") %>'>
                            <asp:ListItem Text ="" Value = "1" ></asp:ListItem>
                            <asp:ListItem Text ="1 min" Value = "1" ></asp:ListItem>
                            <asp:ListItem Text ="5 min" Value = "5" ></asp:ListItem>
                            <asp:ListItem Text ="10 min" Value = "10" ></asp:ListItem>
                            <asp:ListItem Text ="15 min" Value = "15" ></asp:ListItem>
                            <asp:ListItem Text ="30 min" Value = "30" ></asp:ListItem>
                            <asp:ListItem Text ="1 hour" Value = "60" ></asp:ListItem>
                            </asp:DropDownList>
                      </EditItemTemplate>
                  </asp:TemplateField>
                <asp:TemplateField >
                    <HeaderStyle Width="10%" CssClass="gvHeaderCenter"/>
                    <ItemStyle Width="10%" CssClass="gvItemCenter" />
                    <ItemTemplate>
                        <asp:Button ID="btnActivate" runat="server" Text='<%# Eval("Active").ToString()=="1"?"Active":"Disabled" %>' CommandArgument='<%# Eval("testID") %>' CommandName='<%# Eval("Active") %>' ForeColor='<%# Eval("Active").ToString()=="1"?System.Drawing.ColorTranslator.FromHtml("#3DA0D9"):System.Drawing.ColorTranslator.FromHtml("#EE2059") %>' OnClick="btnActivate_Click" CssClass="button-link" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>