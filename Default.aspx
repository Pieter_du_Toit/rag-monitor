﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <table runat="server" style="background-color: #3DA0D9" width="100%">
                <tr>
                    <td style="text-align: center; width: 90%">
                        <asp:LinkButton ID="lnkHeading" runat="server" CssClass="Main-HeaderWhite" OnClientClick="window.location.href='settings.aspx'; return false;"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" runat="server">
                <tr >
                    <td colspan="4" style="vertical-align: bottom; text-align: center;">
                        <asp:LinkButton runat="server" Text="IIS SERVERS" OnClick="btnTestIIS_Click" CssClass="Main-HeaderWhite"/>
                    </td>
                </tr>
                <tr style="vertical-align: Top; align-content: center;" >
                    <td style="width:25%; border-bottom: 0; margin-bottom: 0">
                        <div id="ServerName1" runat="server" class="WhiteCorner">
                            <asp:LinkButton ID="lnkServerName0" runat="server" Text="SERVER 0" OnClick="btnTestServer0_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ServerOneTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divTask1" runat="server" class="divTask whiteBorder"></div>
                        
                        <div id="divPoll1" runat="server" class="divPoll whiteBorder"></div>
                    </td>
                    <td style="width:25%;">
                        <div id="ServerName2" runat="server" class="WhiteCorner">
                            <asp:LinkButton ID="lnkServerName1" runat="server" Text="SERVER 1" OnClick="btnTestServer1_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ServerTwoTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divTask2" runat="server" class="divTask whiteBorder"></div>
                        
                        <div id="divPoll2" runat="server"  class="divPoll whiteBorder"></div>
                    </td>
                    <td style="width: 25%;">
                        <div id="ServerName3" runat="server" class="WhiteCorner">
                            <asp:LinkButton ID="lnkServerName2" runat="server" Text="SERVER 2" OnClick="btnTestServer2_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ServerThreeTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divTask3" runat="server" class="divTask whiteBorder"></div>
                        
                        <div id="divPoll3" runat="server"  class="divPoll whiteBorder"></div>
                    </td>
                    <td style="width: 25%;">
                        <div id="ServerName4" runat="server" class="WhiteCorner">
                            <asp:LinkButton ID="lnkServerName3" runat="server" Text="SERVER 3" OnClick="btnTestServer3_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ServerFourTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divTask4" runat="server" class="divTask whiteBorder"></div>
                        
                        <div id="divPoll4" runat="server" class="divPoll whiteBorder"></div>
                    </td>
                </tr>
                <tr style="vertical-align: Top; align-content: center;" >
                    <td style="width: 25%; text-align:center; align-items: center; align-self: center">
                        <div id="ServerName5" runat="server" class="WhiteCorner">
                            <asp:LinkButton ID="lnkServerName4" runat="server" Text="SERVER 4" OnClick="btnTestServer4_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ServerFiveTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divTask5" runat="server" class="divTask whiteBorder"></div>
                        
                        <div id="divPoll5" runat="server" class="divPoll whiteBorder"></div>
                    </td>
                    <td style="width: 25%; text-align:center; align-items: center; align-self: center">
                        <div id="ServerName6" runat="server" class="WhiteCorner">
                            <asp:LinkButton ID="lnkServerName5" runat="server" Text="SERVER 5" OnClick="btnTestServer5_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ServerSixTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divTask6" runat="server" class="divTask whiteBorder"></div>
                        
                        <div id="divPoll6" runat="server" class="divPoll whiteBorder"></div>
                    </td>
                    <td style="width: 25%; text-align:center; align-items: center; align-self: center" >
                        <div id="ServerName7" runat="server" class="WhiteCorner">
                            <asp:LinkButton ID="lnkServerName6" runat="server" Text="SERVER 6" OnClick="btnTestServer6_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ServerSevenTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divTask7" runat="server" class="divTask whiteBorder"></div>
                        
                        <div id="divPoll7" runat="server" class="divPoll whiteBorder"></div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; border-spacing: 0;">
                <tr style="vertical-align: top; height: 300px;">
                    <td style="width:25%; text-align: center; vertical-align: bottom;" rowspan="2">
                        <div ID="divUSDCHead" runat="server">
                            <asp:LinkButton ID="USDCHeader" runat="server" Text="USDC DOCUMENTS" OnClick="btnTestUsdcDocuments_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="USDCTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divUSDCBlock" runat="server" align="center"></div>
                    </td>
                    <td style="width:25%; text-align: center; vertical-align: bottom;" rowspan="2">
                        <div id="divSpaceAboveDM5" style="height: 16px" runat="server"></div>
                        <div ID="divDM5Head" runat="server">
                            <asp:LinkButton ID="DM5Header" runat="server" Text="DM5" OnClick="btnTestDM5_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="DM5Time" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divDM5Block" runat="server"></div>
                        <div id="divSpaceAboveDBConn" style="height: 16px" runat="server"></div>
                        <div ID="divDBConnHead" runat="server">
                            <asp:LinkButton ID="DbConnectionHeader" runat="server" Text="DB CONNECTIONS" OnClick="btnTestDatabaseConnections_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="DBConnTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divDBConnBlock" runat="server" style="text-align:center;"></div>
                    </td>

                    <td style="width:25%; text-align: center; vertical-align: bottom;">
                        <div id="divJRulesHead" runat="server">
                            <asp:LinkButton ID="JRulesHeader" runat="server" Text="JRULES" OnClick="btnTestJRules_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="JRulesTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divJRulesBlock" runat="server"></div>
                        <div id="divSpaceAboveQAS" style="height: 16px" runat="server"></div>
                        <div id="divQASHead" runat="server">
                            <asp:LinkButton ID="QASHeader" runat="server" Text="QAS" OnClick="btnQAS_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="QASTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divQASBlock" runat="server"></div>
                    </td>

                    <td style="width:25%; text-align: center; vertical-align: bottom;">
                        <div ID="divEDIHead" runat="server">
                            <asp:LinkButton ID="EDIHeader" runat="server" Text="EDI POLLING" OnClick="btnTestEDI_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="EDITime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divEDIBlock" runat="server"></div>
                        <div id="divSpaceAboveThunder" style="height: 16px" runat="server"></div>
                        <div ID="divThunderHead" runat="server">
                            <asp:LinkButton ID="ThunderHeader" runat="server" Text="THUNDERHEAD" OnClick="btnTestThunderhead_Click" CssClass="Main-HeaderWhite"/>
                            <asp:Label ID="ThunderTime" runat="server" Text="" CssClass="Main-HeaderWhiteTime"/>
                        </div>
                        <div id="divThunderBlock" runat="server"></div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Timer runat="server" Interval="5000" OnTick="PageTimer_Tick"></asp:Timer>
</asp:Content>
