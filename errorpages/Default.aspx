﻿<%@ Page Title="Oops" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            
            <table runat="server" style="background-color: #3DA0D9" width="100%">
                <tr>
                    <td style="width:5%; text-align: center"><asp:Image runat="server" ImageUrl="/images/orca.png"/></td>
                    <td style="text-align: center; width: 90%">
                        <asp:Label runat="server" CssClass="Main-HeaderWhite" OnClientClick="window.location.href='../Default.aspx'; return false;">ORCA</asp:Label>
                    </td>
                    <td style="width:5%; text-align: center"></td>
                </tr>
                <tr style="height: 20px;"></tr>
                <tr>
                    <td></td>
                    <td style="text-align: center; width: 90%"><asp:Label runat="server" CssClass="Main-HeaderWhite">Sorry, something went wrong with our web site</asp:Label></td>
                    <td></td>
                </tr>
                <tr style="height: 20px;"></tr>
                <tr>
                    <td></td>
                    <td style="text-align: center; width: 90%">
                        <asp:Label runat="server" CssClass="Main-HeaderWhite">Please </asp:Label>
                        <asp:LinkButton runat="server" CssClass="Main-HeaderWhite-Underlined" OnClientClick="window.location.href='../Default.aspx'; return false;">click here</asp:LinkButton>
                        <asp:Label runat="server" CssClass="Main-HeaderWhite"> to return to the home page</asp:Label>
                    </td>
                    <td></td>
                </tr>
            </table>
            
        </ContentTemplate>
    </asp:UpdatePanel>
        
</asp:Content>
